const jsonServer = require('json-server');
const auth = require('json-server-auth');

const app = jsonServer.create();
const router = jsonServer.router('db.json');

const port = process.env.PORT || 5000;

app.use(function(req, res, next) {
	setTimeout(next, 1000);
});

app.use(jsonServer.defaults());

// /!\ Bind the router db to the app
app.db = router.db;

/*

We add 4 for read permission.
We add 2 for write permission.

First digit are the permissions for the resource owner.
Second digit are the permissions for the logged-in users.
Third digit are the permissions for the public users.

Route	Resource permissions
/664/*	User must be logged to write the resource. 
        Everyone can read the resource.
/660/*	User must be logged to write or read the resource.
/644/*	User must own the resource to write the resource. 
        Everyone can read the resource.
/640/*	User must own the resource to write the resource. 
        User must be logged to read the resource.
/600/*	User must own the resource to write or read the resource.
/444/*	No one can write the resource. 
        Everyone can read the resource.
/440/*	No one can write the resource. 
        User must be logged to read the resource.
/400/*	No one can write the resource. 
        User must own the resource to read the resource.

*/

const rules = auth.rewriter({
	// Permission rules
	//users: 600,
	products: 664,
	categories: 664,
	brands: 664,
	reviews: 664,
	wishList: 600
});

// You must apply the auth middleware before the router
app.use(rules);
app.use(auth);
app.use(router);

app.listen(port, () => {
	console.log('API Server is running....');
});
